import { PostModel } from '../models/PostModel.js';
import { ObjectId } from 'mongodb';

export const getPosts = async (req, res) => {
  console.log("index");
  try {
    
    const posts = await PostModel.find().populate('author');
    
    //console.log('posts', posts);
    res.status(200).json(posts);
  } catch(err) {
    res.status(500).json({ error: err });   
  }
};

export const getPostsBySearch = async (req, res) => {
  try {
    let tag = req.query.s;
    console.log(tag)
    const posts = await PostModel.find({ title: tag}).populate('author');
    res.status(200).json(posts);
  } catch(err) {
    res.status(500).json({ error: err });   
  }
};

export const createPost = async (req, res) => {
  
  try {
    const newPost = req.body;
    newPost.author = new ObjectId(newPost.author);
    const post = new PostModel(newPost);
    await post.save();
    const savedPost = await PostModel.findById(post._id).populate('author');
    res.status(200).json(savedPost); 
  } catch(err) {
    res.status(500).json({ error: err });   
  }
};

export const updatePost = async (req, res) => {
  try {
    const updatePost = req.body;
  
    const post = await PostModel.findOneAndUpdate(
      { _id: updatePost._id }, 
      updatePost, 
      { new: true }
    ).populate('author');
    
    res.status(200).json(post); 
  } catch(err) {
    res.status(500).json({ error: err });   
  }
};

export const deletePost = async (req, res) => {
  
  try {
    const deletePost = req.body;
    const post = await PostModel.findOneAndRemove(
      { _id: deletePost._id }
    );
    res.status(200).json(post); 
  } catch(err) {
    res.status(500).json({ error: err });   
  }
};




